This repository contains data and code for Scott et al. 2023, Royal Society Open Science. 

The files are:

# ESM File 1
Title: Cured Sentinel Cell Counts

Description: These data are sentinel cell counts for D. discoideum hosts that were cured of their Paraburkholderia infections 

Gitlab file name: data/CureFarmerData.txt

# ESM File 2
Title: Infected Sentinel Cell Counts

Description: These data are sentinel cell counts for D. discoideum hosts that were experimentally infected by Paraburkholderia

Gitlab file name: data/InfectNonfarmerData.txt

# ESM File 3
Title: Additional Infected Sentinel Cell Counts

Description: These data are sentinel cell counts for D. discoideum hosts that were experimentally infected by Paraburkholderia

Gitlab file name: data/Nonfarmer_spore_count_data_colonization_with_Pa159_for_R.csv

# ESM File 4
Title: Beads in Sentinel Cells

Description: Bead counts from phagotytosis assay

Gitlab file name: data/Beads_Data.txt

# ESM File 5
Title: Spore Counts After Pathogen Treatment

Description: Spore counts from hosts infected with Paraburkholderia and non-symbiotic pathogens

Gitlab file name: data/SScell2_total_spore_combined.csv

# ESM File 6
Title: Spore Counts From Brock 2016

Description: Spore counts from hosts infected with Paraburkholderia and exposed to EtBr from Brock et al. 2016

Gitlab file name: data/EtBr data for log2Fold change graph.xlsx

# ESM File 7
Title: R Code for Analysis

Description: R code to perform statistical analysis

Gitlab file name: Analysis.R

# How to cite this material
Scott, Trey J. Data and code for Symbiotic bacteria, immune-like sentinel cells, and the response to pathogens in a social amoeba (1.0). Zenodo. https://doi.org/10.5281/zenodo.8189701
